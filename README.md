# zipdoc-pastebin-project



## [집닥] 기술 과제 Pastebin-like service

안녕하세요, 집닥 입니다.먼저 귀중한 시간을 내어 본 과제 과정에 참가해주신 것에 대해 감사드립니다.
본 과제를 통해, 지원자님은 http://pastebin.com/ 과 비슷한 시스템을 설계하고 직접 만드시게 될 것 입니다. 

언어나 프레임워크 등 사용하려는 기술에 대한 제약은 없으며, 본인이 가장 익숙한 것으로 사용해주시면 됩니다.
지원자님께서 제한된 시간 내에 과제를 완성하지 못하실 수 있음을 충분히 인지하고 있으니, 과제 완성 여부에 상관없이 결과를 제출해주시기 바랍니다. 작업 내역은 Git을 통해 관리해주시기 바라며, 작업 이력을 확인할 수 있도록 커밋의 크기를 적절히 조절해주시기 바랍니다. Git Repository는 익숙하신 Git Repository 호스팅 서비스에 올려 저희가 확인 가능한 URL을 공유해주시기 바랍니다.

**위의 과제를 메일로 받으신 이후, 48시간 이내로 URL을 메일로 회신주시기 바랍니다.**

과제를 진행하시면서 궁금한 사항이 있으면 편하게 연락해주시기 바라며, 좋은 결과가 있기를 바랍니다.

본 과제에서 중점적으로 평가할 내용은 다음과 같습니다:

* 시스템 디자인Cache

  * Database
  * Storage
  * Batch Job / ETL Pipeline
  * etc

* API 설계

  * API Interface
  * Data Modeling
  * etc 

* 개발 환경 구성

  * CI/CD Pipeline
  * Linter
  * IaC

  

  **[Use Cases]**

  * 유저는 텍스트를 입력해 Paste를 생성할 수 있고, 텍스트를 볼 수 있는 Paste URL을 얻을 수 있습니다.
    * 만료 (Expiration)
      * 기본적으로는 입력한 Paste는 만료되지 않습니다. 
      * 원하는 경우, 특정 시점에 Paste가 만료되도록 날짜/시간을 설정할 수 있습니다.

  * 유저는 Paste URL에 접속해 입력한 텍스트를 확인할 수 있습니다.
  * 유저는 익명입니다.
  * 서비스는 페이지에 대한 통계를 추적합니다
    * 월간 페이지 방문수

  * 서비스는 만료된 페이스트를 삭제할 수 있습니다
  * 서비스는 고가용성을 만족해야 합니다

  

  위의 과제에서는 아래 기능은 포함하지 않아도 됩니다.

  * **회원가입 및 로그인과 관련된 기능 일체**
  * Paste에 대한 공개/비공개 설정
  * 단축 URL

  

  **[제약사항 및 가정]**

  시스템은 다음 환경에서 안정적으로 서비스 될 수 있도록 설계되어야 합니다.

  * 트래픽은 균일하게 분산되지 않습니다

  * Paste URL을 통해 컨텐츠를 보는 것은 빨라야 합니다.
  * Paste 컨텐츠는 텍스트만 입력 가능합니다. 
  * 이미지나 비디오, 첨부파일과 같은 것은 사용할 수 없습니다.
  * 페이지 뷰에 대한 통계는 실시간으로 연산되거나 표시될 필요가 없습니다.
  * 1백만 사용자 수
  * 월간 1천만회 Paste 쓰기
  * 월간 1억회 Paste 읽기
  * 10:1 의 읽기:쓰기 비율 

  

## Setup & Run

1. Source 다운로드

   ```bash
   $ git https://gitlab.com/hjsix2/zipdoc-pastebin-project.git zipdoc-pastebin-project
   ```

   소스 다운로드 후 의존성 라이브러리들을 다운로드 하고 IDE를 통해 해당 소스를 확인 할 수 있다. 


   

2. npm 설치 및 초기화

   ```ba
   $ npm install
   ```

   javascript 파일은 `ES6` 코드로 작성되어 있어 모듈화에 관련한 의존성 라이브러리를 다운로드 한다.


   

3. Spring Boot Web Application 실행

   ```bash
   $ ./gradlew bootRun
   ```

   

4. JavaScript WebPack Dev Server 실행

   ```bash
   $ npm run develop
   ```

   javascript의 빠른 개발을 위해 변경 사항을 바로 반영/확인 하기 위해 개발시에 proxy server를 기동 한다.

   > javascript 모듈화 및 코드 변환은 다음 명령어를 통해 진행 할 수 있다.
   >
   > ```bash
   > $ npm run build
   > ```

   

5. 브라우져 확인

   Spring boot Web application은 기본 `8080`port로 실행 되나, javascript 코드를 `ES6` 로 작성/확인/디버깅 하기위해서는 webpack dev server 기동후 `9000`port로 접근하면 proxy설정을 통해 개발을 진행 할 수 있다.