'use strict';

/* eslint-disable */
const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const fs = require('fs');
/* eslint-enable */

// eslint-disable-next-line
module.exports = (env, argv) => {
  const entry = {};

  let sourceType = ['content'];
  // eslint-disable-next-line
  let sourcePath = path.resolve(__dirname, 'src/main/js');
  // eslint-disable-next-line
  let buildPath = path.resolve(__dirname, 'src/main/resources/static/js');

  const walkSync = (currentDirPath, type, callback) => {
    fs.readdirSync(currentDirPath).forEach(name => {
      let filePath = path.join(currentDirPath, name);
      let stat = fs.statSync(filePath);
      if (stat.isFile()) {
        callback(filePath, name);
      } else if (stat.isDirectory() && name === type) {
        walkSync(filePath, type, callback);
      }
    });
  };

  sourceType.forEach((element) => {
    walkSync(sourcePath, element, (filePath, name) => {
      entry[`${element}/${name.replace('.js', '.min')}`] = ['babel-polyfill', filePath];
    });
  });

  let config = {
    entry,
    output: {
      path: buildPath,
      filename: '[name].js'
    },
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }, {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      }]
    },
    plugins: [
    ],
    stats: {
      colors: true
    }
  };

  if (argv.mode === 'development') {
    const url = '0.0.0.0'; 
    const protocol = 'http'; 
    const devPort = 9000;
    const proxyPort = 8080;
    const demoEntry = {};

    config.entry = Object.assign({}, demoEntry, config.entry);
    config.devtool = 'inline-source-map';
    config.devServer = {
      host: url,
      port: devPort,
      hot: true,
      publicPath: '/js/',
      inline: true,
      historyApiFallback: false,
      proxy: {
        '!/js/content/**': {
          target: `${protocol}://${url}:${proxyPort}`,
          secure: false,
          changeOrigin: true,
          bypass: function(req) {
            if (req.originalUrl.indexOf('hot-update') !== -1) {
              let update = req.originalUrl.split('/');
              let type = update[update.length - 2];
              let updateFile = update[update.length -1];
              return `/js/content/${(updateFile.indexOf('json') !== -1 ? '' : `${type}/`)}${updateFile}`;
            }
          }
        }
      }
    };

    Object.keys(config.entry).forEach(key => {
      config.entry[key].push(`webpack-dev-server/client?${protocol}://${url}:${devPort}`);
      config.entry[key].push('webpack/hot/only-dev-server');
    });

    config.plugins.push(new webpack.HotModuleReplacementPlugin());
  } else {
    config.plugins.push(new CleanWebpackPlugin([`${buildPath}/content/*.*`]));
  }
  
  return config;
};
