#!/bin/bash
GROUP="zipdoc"
DOCKER_REGISTRY="localhost:5000/$GROUP"
PROJECT="$GROUP-pastebin-project"
PROFILE="production"
RELEASE_VERSION="RELEASE"
TAG=$1


echo "DOCKER_REGISTRY : $DOCKER_REGISTRY, PROFILE : $PROFILE, PROJECT : $PROJECT, TAG : $TAG"


function build() {
  if [[ "$TAG" == *"$RELEASE_VERSION"* ]];
  then
      docker build --build-arg VERSION=${TAG} -t ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT} ${PROJECT}/build/docker/${PROFILE}/
  else
      docker build -t ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT} build/docker/${PROFILE}/
  fi

	echo -e "docker images build !! \n\n"
}

function tagging() {
	docker tag ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT} ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT}:${TAG}

	echo -e "docker images tagging !! \n\n"
}

function release() {
  docker push ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT}:latest
	docker push ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT}:${TAG}

	echo -e "docker images registry push !! \n\n"
}

function cleanup() {
  docker rmi ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT}:latest
	docker rmi ${DOCKER_REGISTRY}/${PROFILE}/${PROJECT}:${TAG}

	echo -e "docker images clean !! \n\n"
}

build
tagging
release
cleanup
