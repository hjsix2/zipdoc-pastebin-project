#!/bin/bash
GROUP="zipdoc"
PROJECT="$GROUP-pastebin-project"

PROJECT_PATH="/data/docker"
LOGPATH="/data/logs"

whatToFind="Started "
bufferMessage="Buffering: "
startedMessage="Application Started... exiting buffer!"

function pull() {
	echo ""
	docker-compose pull ${PROJECT}
	echo ""
}

function stop() {
  docker-compose stop ${PROJECT}
  echo ""
}

function run() {
  docker-compose up -d ${PROJECT}
  echo ""
  sleep 5
}

function clean() {
  none=$(docker images -f dangling=true -q)
  if [ "$none" ];
  then
      docker rmi `docker images -f dangling=true -q`
  fi
  echo ""
}

function watch() {
  logFile=$(ls -td1 ${LOGPATH}/*.log | head -1)
  echo "LOG : $logFile"
  tail -f ${logFile} |
  while IFS= read line
  do
    echo "$line"
    if [[ "$line" == *"$whatToFind"* ]]; then
      echo ${startedMessage}
      pkill tail
    fi
  done
}

cd ${PROJECT_PATH}
pull
stop
run
clean
watch
