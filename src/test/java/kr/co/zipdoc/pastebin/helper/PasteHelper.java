package kr.co.zipdoc.pastebin.helper;

import kr.co.zipdoc.pastebin.domain.PasteDto;
import kr.co.zipdoc.pastebin.domain.entity.MockEntity;
import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.domain.enumeration.Expiration;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class PasteHelper {

  public static Paste getEntity(Long id) {
    return MockEntity.mock(Paste.builder().title("test title")
        .content("test content")
        .expiration(Expiration.NAVER)
        .build(), id);
  }

  public static PasteDto.Create getCreate() {
    return new PasteDto.Create("test title", Expiration.NAVER, "test content");
  }

}
