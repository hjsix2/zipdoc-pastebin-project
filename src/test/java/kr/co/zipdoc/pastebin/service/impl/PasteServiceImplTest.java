package kr.co.zipdoc.pastebin.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import kr.co.zipdoc.pastebin.common.component.MessageComponent;
import kr.co.zipdoc.pastebin.common.exception.ConflictException;
import kr.co.zipdoc.pastebin.domain.PasteDto;
import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.helper.PasteHelper;
import kr.co.zipdoc.pastebin.repository.PasteRepository;
import kr.co.zipdoc.pastebin.service.PasteService;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PasteServiceImpl.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class PasteServiceImplTest {

  private static final Long ID = 1L;

  private Paste mock = PasteHelper.getEntity(ID);

  @Autowired
  PasteService service;

  @MockBean
  PasteRepository repository;

  @MockBean
  MessageComponent messageComponent;

  @Rule
  public ExpectedException exceptionRule = ExpectedException.none();


  @Test
  public void set() {
    doReturn(this.mock).when(repository).save(any(Paste.class));
    PasteDto.Create create = PasteHelper.getCreate();

    Paste paste = service.set(create);
    assertThat(paste).isNotNull();
    assertThat(paste.getTitle()).isEqualTo(create.getTitle());
    assertThat(paste.getContent()).isEqualTo(create.getContent());
    assertThat(paste.getExpiration()).isEqualTo(create.getExpiration());
    assertThat(paste.getExpirationAt()).isNull();

    verify(repository, times(1)).save(isA(Paste.class));
    verifyNoMoreInteractions(repository);
  }

  @Test
  public void whenExceptionThrownRemove() {
    doReturn(Optional.of(this.mock)).when(repository).findById(anyLong());

    exceptionRule.expect(ConflictException.class);

    service.remove(1L);

    verify(repository, times(1)).findById(isA(Long.class));
    verifyNoMoreInteractions(repository);
  }

}
