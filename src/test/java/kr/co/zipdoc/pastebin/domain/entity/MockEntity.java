package kr.co.zipdoc.pastebin.domain.entity;

import java.time.LocalDateTime;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class MockEntity extends AbstractEntity {

  private static final long serialVersionUID = 8682848890017038030L;

  public static <T extends AbstractEntity> T mock(Class<T> clazz, long id) {
    try {
      T mock = clazz.newInstance();
      mock.id = id;
      mock.updateAt = LocalDateTime.now();
      mock.createAt = LocalDateTime.now();

      return mock;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  public static <T extends AbstractEntity> T mock(T mock, long id) {
    try {
      mock.id = id;
      mock.updateAt = LocalDateTime.now();
      mock.createAt = LocalDateTime.now();

      return mock;
    } catch (Exception e) {
      e.printStackTrace();
      return mock;
    }
  }

}
