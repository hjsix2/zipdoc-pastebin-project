package kr.co.zipdoc.pastebin.common.configuration.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ControllerAdvice
public class CollectionValidtorControllerAdvice {

  private final LocalValidatorFactoryBean localValidatorFactoryBean;

  @Autowired
  public CollectionValidtorControllerAdvice(@Qualifier("validatorFactoryBean") LocalValidatorFactoryBean localValidatorFactoryBean) {
    this.localValidatorFactoryBean = localValidatorFactoryBean;
  }

  @InitBinder
  public void initBinder(WebDataBinder binder) {
    binder.addValidators(new CollectionValidator(localValidatorFactoryBean));
  }

}
