package kr.co.zipdoc.pastebin.common.configuration.web;

import java.nio.charset.StandardCharsets;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Configuration
public class CharacterEncodingConfiguration {

  @SuppressWarnings("java:S3740")
  @Bean
  public FilterRegistrationBean characterEncodingFilter() {
    FilterRegistrationBean registrationBean = new FilterRegistrationBean();
    registrationBean.setFilter(new CharacterEncodingFilter(StandardCharsets.UTF_8.name(), true));

    return registrationBean;
  }

}
