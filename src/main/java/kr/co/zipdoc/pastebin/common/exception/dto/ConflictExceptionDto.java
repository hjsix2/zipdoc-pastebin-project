package kr.co.zipdoc.pastebin.common.exception.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ConflictExceptionDto extends ExceptionDto {

  private static final long serialVersionUID = 1L;

  public ConflictExceptionDto(int status, String error, String message, String exception, String path) {
    super(status, error, exception, message, path);
  }

}
