package kr.co.zipdoc.pastebin.common.configuration.web;

import java.nio.charset.StandardCharsets;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Configuration
public class HttpMessageConverterConfiguration {

  @Bean
  public HttpMessageConverter<String> responseBodyConverter() {
    return new StringHttpMessageConverter(StandardCharsets.UTF_8);
  }

}
