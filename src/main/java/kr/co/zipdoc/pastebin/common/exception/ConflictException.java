package kr.co.zipdoc.pastebin.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@ResponseStatus(value = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ConflictException() {
    super("Conflict error!!");
  }

  public ConflictException(String message) {
    super(message);
  }

}
