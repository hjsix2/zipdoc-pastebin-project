package kr.co.zipdoc.pastebin.common.configuration.jpa;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EntityScan(basePackageClasses = {Jsr310JpaConverters.class}, basePackages = {"kr.co.zipdoc.pastebin.domain"})
@EnableJpaRepositories(basePackages = {"kr.co.zipdoc.pastebin.repository"})
public class JpaConfiguration {

  @Bean
  public AuditorAware<String> auditorAware() {
    return new AuditorAwareImpl();
  }

}
