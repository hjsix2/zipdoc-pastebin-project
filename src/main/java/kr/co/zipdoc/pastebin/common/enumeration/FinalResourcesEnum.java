package kr.co.zipdoc.pastebin.common.enumeration;

import java.util.Arrays;
import java.util.stream.Stream;

import kr.co.zipdoc.pastebin.common.util.UriUtils;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public enum FinalResourcesEnum {

  ALL("/**"), STATIC("/static"), WEBJARS("/webjars"), H2("/h2-console"), ERROR("/error");

  private final String path;

  FinalResourcesEnum(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  public String getPathAll() {
    return path + ALL.path;
  }

  public String buildPath(String... pathSegments) {
    return UriUtils.buildPath(Stream.concat(Stream.of(path), Arrays.stream(pathSegments)).toArray(String[]::new));
  }

}
