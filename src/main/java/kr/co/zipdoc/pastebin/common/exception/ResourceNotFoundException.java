package kr.co.zipdoc.pastebin.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ResourceNotFoundException() {
    super("The requested page could not be found.");
  }

  public ResourceNotFoundException(String message) {
    super(message);
  }

}
