package kr.co.zipdoc.pastebin.common.util;

import java.util.Collection;
import java.util.Map;

public class ObjectUtils {

  private ObjectUtils() {
    throw new IllegalStateException("Utility class");
  }

  public static boolean isCollection(Object object) {
    return Collection.class.isAssignableFrom(object.getClass()) || Map.class.isAssignableFrom(object.getClass());
  }

}
