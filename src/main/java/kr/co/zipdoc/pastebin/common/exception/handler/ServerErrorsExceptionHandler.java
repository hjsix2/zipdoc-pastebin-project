package kr.co.zipdoc.pastebin.common.exception.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ControllerAdvice
public class ServerErrorsExceptionHandler {

  @ExceptionHandler({NestedRuntimeException.class})
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public void handleNestedRuntimeException(RuntimeException ex, HttpServletResponse response) throws IOException {
    response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), StringUtils.substringBefore(ex.getMessage(), ".").replaceAll("\n###", ""));
  }

}
