package kr.co.zipdoc.pastebin.common.component.scheduler;

import org.springframework.util.StopWatch;

import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
public class CommonSchedulerWorker {

  StopWatch start(String id) {
    StopWatch stopWatch = new StopWatch(id);
    stopWatch.start(id + " scheduler initializing");

    log.info("::::: scheduler : {} Scheduler Start", id);

    return stopWatch;
  }

  void stop(StopWatch stopWatch) {
    stopWatch.stop();
    log.info("::::: scheduler : {} Scheduler End - run time : {}", stopWatch.getId(), stopWatch.getTotalTimeSeconds());
  }

}
