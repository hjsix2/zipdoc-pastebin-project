package kr.co.zipdoc.pastebin.common.exception.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;

import kr.co.zipdoc.pastebin.common.exception.ConflictException;
import kr.co.zipdoc.pastebin.common.exception.dto.ConflictExceptionDto;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ControllerAdvice
public class ClientErrorsExceptionHandler {

  @ExceptionHandler(value = {HttpMessageNotReadableException.class, MissingServletRequestParameterException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public void handleNotAuthenticated(Exception ex, WebRequest request, HttpServletResponse response) throws IOException {
    response.sendError(HttpStatus.BAD_REQUEST.value(), "Invalid request.");
  }

  @ExceptionHandler(ConflictException.class)
  @ResponseStatus(HttpStatus.CONFLICT)
  @ResponseBody
  public ConflictExceptionDto handleConflictError(ConflictException ex, HttpServletRequest request) {
    return new ConflictExceptionDto(HttpStatus.CONFLICT.value(), HttpStatus.CONFLICT.getReasonPhrase(), this.getClass().getName(), ex.getMessage(), request.getRequestURI());
  }

  @ExceptionHandler(value = {MultipartException.class})
  @ResponseBody
  @ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
  public Object handleMultipartException(Exception ex, WebRequest request, HttpServletResponse response) throws IOException {
    if (isJsonFormatForResponse(request)) {
      response.sendError(HttpStatus.PAYLOAD_TOO_LARGE.value());
      return null;
    }

    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("status", HttpStatus.PAYLOAD_TOO_LARGE.value());
    modelAndView.addObject("error", HttpStatus.PAYLOAD_TOO_LARGE.getReasonPhrase());
    modelAndView.setViewName("error/error");
    return modelAndView;
  }

  private boolean isJsonFormatForResponse(WebRequest request) {
    String contentType = request.getHeader("content-type");

    if (contentType == null) {
      return false;
    }

    String accept = request.getHeader("accept");

    boolean isApplicationJsonRequest = contentType.contains(MediaType.APPLICATION_JSON_VALUE);
    boolean isContainedAccpetApplicationJson = accept.contains(MediaType.APPLICATION_JSON_VALUE);

    return isApplicationJsonRequest || isContainedAccpetApplicationJson;
  }

}
