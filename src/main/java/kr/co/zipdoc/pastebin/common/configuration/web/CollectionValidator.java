package kr.co.zipdoc.pastebin.common.configuration.web;

import java.util.Collection;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import kr.co.zipdoc.pastebin.common.util.ObjectUtils;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class CollectionValidator implements Validator {

  private final Validator validator;

  CollectionValidator(LocalValidatorFactoryBean validatorFactory) {
    this.validator = validatorFactory;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }

  @Override
  public void validate(Object target, Errors errors) {
    if (ObjectUtils.isCollection(target)) {
      ((Collection<?>) target)
        .forEach(object -> ValidationUtils.invokeValidator(validator, object, errors));
    }
  }

}
