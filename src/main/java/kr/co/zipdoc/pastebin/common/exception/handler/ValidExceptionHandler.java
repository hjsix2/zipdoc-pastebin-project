package kr.co.zipdoc.pastebin.common.exception.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Path.Node;

import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import kr.co.zipdoc.pastebin.common.component.MessageComponent;
import kr.co.zipdoc.pastebin.common.exception.ValidationException;
import kr.co.zipdoc.pastebin.common.exception.dto.FieldErrorDto;
import kr.co.zipdoc.pastebin.common.exception.dto.ValidationExceptionDto;
import lombok.RequiredArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@ControllerAdvice
@RequiredArgsConstructor
public class ValidExceptionHandler {

  private final MessageComponent messageComponent;


  @ExceptionHandler(BindException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationExceptionDto handleValidationError(BindException ex, HttpServletRequest request) {
    return getBindingMessages(ex.getFieldErrors(), ex.getClass().getName(), request.getRequestURI());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationExceptionDto handleValidationError(MethodArgumentNotValidException ex, HttpServletRequest request) {
    return getBindingMessages(ex.getBindingResult().getFieldErrors(), ex.getClass().getName(), request.getRequestURI());
  }

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationExceptionDto handleValidationError(ConstraintViolationException ex, HttpServletRequest request) {
    ValidationExceptionDto validationExceptionDto = new ValidationExceptionDto(ex.getClass().getName(), "", request.getRequestURI());
    validationExceptionDto.setErrors(ex.getConstraintViolations().stream().map(e -> {
      List<Node> nodes = new ArrayList<>();
      e.getPropertyPath().forEach(nodes::add);

      String field = nodes.stream().map(Node::toString).reduce((a, b) -> b).orElseGet(String::new);
      String code = e.getConstraintDescriptor().getAnnotation().annotationType().getName();
      String key = "validated.error." + field + "." + code.substring(code.lastIndexOf('.') + 1).toLowerCase();

      return new FieldErrorDto(e.getMessage(), field, e.getInvalidValue(), key);
    }).collect(Collectors.toList()));

    validationExceptionDto.setMessage("Validation failed. Error count: " + validationExceptionDto.getErrors().size());

    return validationExceptionDto;
  }

  @ExceptionHandler({ValidationException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ValidationExceptionDto handleVaildationException(ValidationException ex, HttpServletRequest request) {
    ValidationExceptionDto validationExceptionDto = new ValidationExceptionDto(ex.getClass().getName(), "", request.getRequestURI());

    if (!ObjectUtils.isEmpty(ex.getError())) {
      validationExceptionDto.addFieldErrors(ex.getError());
    }

    validationExceptionDto.setMessage(ex.getMessage());

    return validationExceptionDto;
  }

  private ValidationExceptionDto getBindingMessages(List<FieldError> fieldErrors, String exception, String path) {
    String message = "Validation failed. Error count: " + fieldErrors.size();
    ValidationExceptionDto validationExceptionDto = new ValidationExceptionDto(exception, message, path);

    fieldErrors.stream().filter(Objects::nonNull).forEach((FieldError fieldError) -> {
      String defaultMessage = messageComponent.getMessage(fieldError);
      String key = "valid.error." + fieldError.getField() + "." + fieldError.getCode();
      Object[] arguments = Arrays.copyOfRange(fieldError.getArguments(), 1, fieldError.getArguments().length);

      validationExceptionDto.addFieldErrors(new FieldErrorDto(defaultMessage, fieldError.getObjectName(), fieldError.getField(), fieldError.getRejectedValue(), key.toLowerCase(), arguments));
    });

    return validationExceptionDto;
  }

}
