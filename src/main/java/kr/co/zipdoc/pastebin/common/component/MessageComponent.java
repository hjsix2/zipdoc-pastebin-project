package kr.co.zipdoc.pastebin.common.component;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Component
public class MessageComponent {

  private final MessageSource messageSource;

  @Autowired
  public MessageComponent(MessageSource messageSource) {
    this.messageSource = messageSource;
  }

  public String getMessage(String code) {
    return getMessage(code, null, getLocale());
  }

  public String getMessage(String code, Locale locale) {
    return getMessage(code, null, locale);
  }

  public String getMessage(String code, Object... args) {
    return getMessage(code, args, getLocale());
  }

  public String getMessage(String code, Object[] args, Locale locale) {
    return messageSource.getMessage(code, args, locale);
  }

  public String getMessage(String code, Object[] args, Locale locale, String defaultMessage) {
    return messageSource.getMessage(code, args, defaultMessage, locale);
  }

  public String getMessage(FieldError fieldError) {
    return messageSource.getMessage(fieldError, getLocale());
  }

  private Locale getLocale() {
    return LocaleContextHolder.getLocale();
  }

}
