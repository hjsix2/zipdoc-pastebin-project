package kr.co.zipdoc.pastebin.common.component.scheduler;

import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.support.collections.DefaultRedisList;
import org.springframework.integration.redis.util.RedisLockRegistry;
import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
@Profile(value = {"production"})
@Configuration
public class SchedulerRedisConfiguration {

  public static final String KEY = "queue";

  @Bean
  StringRedisTemplate stringRedisTemplate(RedisConnectionFactory connectionFactory) {
    return new StringRedisTemplate(connectionFactory);
  }

  @Bean
  RedisLockRegistry lockRegistry(RedisConnectionFactory connectionFactory) {
    return new RedisLockRegistry(connectionFactory, KEY);
  }

  @Bean
  DefaultRedisList<SampleTask> queue(RedisTemplate<String, SampleTask> redisTemplate) {
    DefaultRedisList<SampleTask> queue = new DefaultRedisList<>(KEY, redisTemplate.opsForList().getOperations());
    queue.expire(60, TimeUnit.SECONDS);
    return queue;
  }

  @Bean
  RedisTemplate<String, SampleTask> redisTemplate(RedisConnectionFactory connectionFactory) {
    RedisTemplate<String, SampleTask> template = new RedisTemplate<>();
    template.setConnectionFactory(connectionFactory);
    template.afterPropertiesSet();
    return template;
  }

  @Bean
  RedisMessageListenerContainer redisMessageListenerContainer(RedisConnectionFactory connectionFactory) {
    RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
    redisMessageListenerContainer.setConnectionFactory(connectionFactory);
    registEchoChannel(redisMessageListenerContainer);
    return redisMessageListenerContainer;
  }

  private void registEchoChannel(RedisMessageListenerContainer redisMessageListenerContainer) {
    redisMessageListenerContainer.addMessageListener(new MessageListener() {

      @Override
      public void onMessage(Message message, byte[] pattern) {
        log.debug("[{}]\n{}", new String(message.getChannel()), new String(message.getBody()));
      }
    }, new PatternTopic("agent#sa*"));

  }
}
