package kr.co.zipdoc.pastebin.common.util;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.util.Assert;
import org.springframework.web.util.UriComponentsBuilder;

public class UriUtils {

  private UriUtils() {
    throw new IllegalStateException("Utility class");
  }


  public static String build(String httpUrl, String... pathSegments) {
    return UriComponentsBuilder.fromHttpUrl(httpUrl).pathSegment(pathSegments).build().toUriString();
  }

  public static String buildPath(String... pathSegments) {
    Assert.notEmpty(pathSegments, "pathSegment required");
    Optional<String> uri = Arrays.stream(pathSegments).filter(p -> UrlValidator.getInstance().isValid(p)).findFirst();

    return uri.map(s -> build(s, Arrays.stream(pathSegments).filter(p -> !UrlValidator.getInstance().isValid(p)).toArray(String[]::new)))
        .orElseGet(() -> Objects.requireNonNull(UriComponentsBuilder.fromPath(String.join("/", pathSegments)).build().getPath()).replaceAll("/+", "/"));

  }

  public static String removeEndSlash(String url) {
    return url.replaceAll("/$", "");
  }

}
