package kr.co.zipdoc.pastebin.common.exception.dto;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
public class ExceptionDto implements Serializable {

  private static final long serialVersionUID = -5502597644743161931L;

  private long timestamp = System.currentTimeMillis();
  private int status;
  private String error;
  private String exception;
  private String message;
  private String path;

  public ExceptionDto(int status, String error, String message, String exception, String path) {
    this.status = status;
    this.error = error;
    this.message = message;
    this.exception = exception;
    this.path = path;
  }

}
