package kr.co.zipdoc.pastebin.common.exception.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class FieldErrorDto implements Serializable {

  private static final long serialVersionUID = 7568920743894767685L;

  private String message;
  private String objectName;
  private String field;
  private transient Object rejectedValue;
  private String key;
  private transient Object[] arguments;

  public FieldErrorDto(String message, String field, Object rejectedValue, String key) {
    this.message = message;
    this.field = field;
    this.rejectedValue = rejectedValue;
    this.key = key;
  }

  public FieldErrorDto(String message, String objectName, String field, Object rejectedValue, String key) {
    this.message = message;
    this.objectName = objectName.toLowerCase();
    this.field = field;
    this.rejectedValue = rejectedValue;
    this.key = key;
  }

  public FieldErrorDto(String message, String objectName, String field, Object rejectedValue, String key, Object... arguments) {
    this.message = message;
    this.objectName = objectName.toLowerCase();
    this.field = field;
    this.rejectedValue = rejectedValue;
    this.arguments = arguments;
    this.key = key;
  }

}
