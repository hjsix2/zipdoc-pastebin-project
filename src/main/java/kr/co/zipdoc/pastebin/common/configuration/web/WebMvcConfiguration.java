package kr.co.zipdoc.pastebin.common.configuration.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.HttpStatus;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;

import kr.co.zipdoc.pastebin.common.enumeration.FinalResourcesEnum;
import kr.co.zipdoc.pastebin.common.exception.ValidationExceptionErrorAttributes;
import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
@Configuration
@Import(ThymeleafAutoConfiguration.class)
public class WebMvcConfiguration implements WebMvcConfigurer {

  private final MessageSource messageSource;

  @Value("${spring.h2.console.enabled:false}")
  private boolean h2ConsoleEnabled;

  @Value("${spring.h2.console.path:/h2-console}")
  private String h2ConsolePath;

  @Autowired
  public WebMvcConfiguration(MessageSource messageSource) {
    super();
    this.messageSource = messageSource;
  }


  @Override
  public void addInterceptors(InterceptorRegistry registry) {
  }

  @Override
  public Validator getValidator() {
    return localValidatorFactoryBean();
  }

  @Bean(name = "validatorFactoryBean")
  public LocalValidatorFactoryBean localValidatorFactoryBean() {
    final LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
    localValidatorFactoryBean.setValidationMessageSource(this.messageSource);

    return localValidatorFactoryBean;
  }

  @Bean
  public MethodValidationPostProcessor methodValidationPostProcessor() {
    MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
    methodValidationPostProcessor.setValidator(localValidatorFactoryBean());

    return methodValidationPostProcessor;
  }

  @Override
  public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    argumentResolvers.add(getPageableHandlerMethodArgumentResolver());
  }

  private PageableHandlerMethodArgumentResolver getPageableHandlerMethodArgumentResolver() {
    PageableHandlerMethodArgumentResolver pageableHandlerMethodArgumentResolver = new PageableHandlerMethodArgumentResolver();
    pageableHandlerMethodArgumentResolver.setOneIndexedParameters(true);
    return pageableHandlerMethodArgumentResolver;
  }

  @Bean
  public Java8TimeDialect java8TimeDialect() {
    return new Java8TimeDialect();
  }

  @Bean
  public ErrorViewResolver errorViewResolver() {
    return (HttpServletRequest request, HttpStatus status, Map<String, Object> model) -> {
      ModelAndView modelAndView = new ModelAndView();

      switch (HttpStatus.valueOf(status.value())) {
        case UNAUTHORIZED:
        case FORBIDDEN:
        case GONE:
        case BAD_REQUEST:
        case NOT_FOUND:
          modelAndView.setViewName(getErrorView(HttpStatus.NOT_FOUND));
          break;
        default:
          modelAndView.setViewName(getErrorView(HttpStatus.INTERNAL_SERVER_ERROR));
          break;
      }

      modelAndView.addObject("status", status.value());
      modelAndView.addObject("error", status.getReasonPhrase());

      return modelAndView;
    };
  }

  private String getErrorView(HttpStatus status) {
    StringBuilder fileName = new StringBuilder("error-");
    String viewName = FinalResourcesEnum.ERROR.buildPath(fileName.append(status.value()).toString());

    if (viewName.charAt(0) == '/') {
      return viewName.substring(1);
    }

    return viewName;
  }

  @Bean
  public ValidationExceptionErrorAttributes errorAttributes() {
    return new ValidationExceptionErrorAttributes();
  }

}
