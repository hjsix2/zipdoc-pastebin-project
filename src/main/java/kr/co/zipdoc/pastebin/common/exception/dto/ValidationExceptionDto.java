package kr.co.zipdoc.pastebin.common.exception.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ValidationExceptionDto extends ExceptionDto {

  private static final long serialVersionUID = 2792394257997819581L;

  private List<FieldErrorDto> errors = new ArrayList<>();

  public ValidationExceptionDto(String exception, String message, String path) {
    super(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(), exception, message, path);
  }

  public void addFieldErrors(FieldErrorDto error) {
    errors.add(error);
  }

}
