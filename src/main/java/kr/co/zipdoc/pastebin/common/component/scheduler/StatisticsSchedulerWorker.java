package kr.co.zipdoc.pastebin.common.component.scheduler;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.support.collections.DefaultRedisList;
import org.springframework.integration.redis.util.RedisLockRegistry;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import kr.co.zipdoc.pastebin.service.PastePageViewService;
import kr.co.zipdoc.pastebin.service.PasteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
@Profile(value = {"production"})
@Component
@RequiredArgsConstructor
public class StatisticsSchedulerWorker extends CommonSchedulerWorker {

  private final PastePageViewService pastePageViewService;
  private final PasteService pasteService;

  private final RedisConnectionFactory redisConnectionFactory;
  private final DefaultRedisList<SampleTask> queue;
  private final RedisLockRegistry lockRegistry;


  @Scheduled(cron = "0 0 4 * * *")
  public void addRemovePastePageWork() throws InterruptedException {
    String id = "[daily] Page Remove Task Queue Add";

    StopWatch stopWatch = start(id);

    Lock lock = lockRegistry.obtain(SchedulerRedisConfiguration.KEY);
    if (lock.tryLock(500, TimeUnit.MILLISECONDS)) {
      try {
        SampleTask task = new SampleTask("removePaste");
        if (!queue.contains(task)) {
          queue.add(task);
        }
      } finally {
        lock.unlock();
        logging(queue.iterator());
      }
    }

    stop(stopWatch);
  }

  // TODO Batch로 변경 처리
  @Scheduled(cron = "0 0 5 * * *")
  public void runRemovePastePageWork() {
    String id = "[daily] Page Remove";

    StopWatch stopWatch = start(id);

    SampleTask currentJob = queue.poll();
    if (currentJob != null && currentJob.isKey() && currentJob.getKey().equals("removePaste")) {
      log.info("::::: run {}", currentJob.getKey());
      pasteService.remove();
    }

    stop(stopWatch);
  }

  @Scheduled(cron = "0 10 0 1 * *")
  public void putSetStatisticsMonthlyPastePageViewWork() throws InterruptedException {
    String id = "[month] Page View Statistics Task Queue Add";

    StopWatch stopWatch = start(id);

    Lock lock = lockRegistry.obtain(SchedulerRedisConfiguration.KEY);
    if (lock.tryLock(500, TimeUnit.MILLISECONDS)) {
      try {
        SampleTask task = new SampleTask("statisticsPastePageView");
        if (!queue.contains(task)) {
          queue.add(task);
        }
      } finally {
        lock.unlock();
        logging(queue.iterator());
      }
    }

    pastePageViewService.runBatch(LocalDate.now());

    stop(stopWatch);
  }

  // TODO pv 생성 규직을 명확히 하여 - Spring Batch로 변경 처리
  @Scheduled(cron = "0 0 1 1 * *")
  public void runSetStatisticsMonthlyPastePageViewWork() throws InterruptedException {
    String id = "[month] Page View Statistics";

    StopWatch stopWatch = start(id);

    SampleTask currentJob = queue.poll();
    if (currentJob != null && currentJob.isKey() && currentJob.getKey().equals("statisticsPastePageView")) {
      log.info("::::: run {}", currentJob.getKey());
      pastePageViewService.runBatch(LocalDate.now());
    }

    stop(stopWatch);
  }

  private void logging(Iterator<SampleTask> values) {
    StringBuilder builder = new StringBuilder();
    while (values.hasNext()) {
      builder.append(values.next().toString()).append(" ");
    }

    log.info("::::: in : {}", builder.toString());
  }

}
