package kr.co.zipdoc.pastebin.common.configuration.web;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import lombok.RequiredArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Profile(value = {"production"})
@Configuration
@RequiredArgsConstructor
public class HttpSessionConfiguration {

  @Value("${server.servlet.session.timeout:3600}")
  private int maxInactiveIntervalInSeconds;

  private final RedisOperationsSessionRepository sessionRepository;

  @PostConstruct
  private void afterPropertiesSet() {
    sessionRepository.setDefaultMaxInactiveInterval(maxInactiveIntervalInSeconds);
  }

  @Bean
  public CookieSerializer cookieSerializer() {
    DefaultCookieSerializer serializer = new DefaultCookieSerializer();
    serializer.setCookieName("R_JSESSIONID_ZIPDOC_PASTEBIN_APPLICATION");
    serializer.setCookiePath("/");
    serializer.setDomainNamePattern("^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\\\.)+[A-Za-z]{2,6}$");
    return serializer;
  }

}
