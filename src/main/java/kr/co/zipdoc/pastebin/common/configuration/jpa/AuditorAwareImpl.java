package kr.co.zipdoc.pastebin.common.configuration.jpa;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class AuditorAwareImpl implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    return Optional.of("anonymous");
  }

}
