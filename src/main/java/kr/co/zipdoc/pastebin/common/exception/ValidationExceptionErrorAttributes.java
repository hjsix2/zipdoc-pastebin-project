package kr.co.zipdoc.pastebin.common.exception;

import java.util.Collections;
import java.util.Map;

import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.util.ObjectUtils;
import org.springframework.web.context.request.WebRequest;

import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
public class ValidationExceptionErrorAttributes extends DefaultErrorAttributes {

  @Override
  public Map<String, Object> getErrorAttributes(WebRequest webRequest, boolean includeStackTrace) {
    Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, includeStackTrace);

    Throwable error = getError(webRequest);
    if (error != null) {
      ValidationException result = extractValidationExceptionResult(error);
      if (result == null) {
        return errorAttributes;
      }

      if (!ObjectUtils.isEmpty(result.getError())) {
        errorAttributes.put("errors", Collections.singletonList(result.getError()));
        errorAttributes.put("message", "Validation failed for object='" + result.getError().getObjectName() + "'. Error count: 1");
      }
    }

    return errorAttributes;
  }

  private ValidationException extractValidationExceptionResult(Throwable error) {
    if (error instanceof ValidationException) {
      return (ValidationException) error;
    }

    return null;
  }

}
