package kr.co.zipdoc.pastebin.common.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import kr.co.zipdoc.pastebin.domain.entity.PastePageView;
import kr.co.zipdoc.pastebin.service.PastePageViewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class PastePageViewAspectComponent {

  private final PastePageViewService service;

  @Pointcut("execution(* kr.co.zipdoc.pastebin.controller..*(..)) && @annotation(org.springframework.web.bind.annotation.GetMapping) && @annotation(kr.co.zipdoc.pastebin.common.component.PageViewLogging)")
  public void excute() {
  }

  // TODO RDB 처리가 아닌 elasticsearch 등으로 처리.
  @Around(value = "excute() && args(request,response,model,id,..)", argNames = "joinPoint,request,response,model,id")
  public Object around(ProceedingJoinPoint joinPoint, HttpServletRequest request, HttpServletResponse response, Model model, Long id) throws Throwable {

    Object proceed = joinPoint.proceed(joinPoint.getArgs());

    service.set(new Description(request, response).toEntity(id));

    return proceed;
  }

  // TODO client 정보를 세분화 하여 조회 할 필요가 있음.
  @SuppressWarnings("squid:S2089")
  public static class Description {
    String patterns = "";
    String referer = "";
    String methods = "";
    String consumes = "";
    String produces = "";
    String params = "";
    String requestIp = "";

    Description(HttpServletRequest request, HttpServletResponse response) {
      this.patterns = request.getServletPath();
      this.referer = request.getHeader("referer");
      this.methods = request.getMethod();
      this.consumes = request.getContentType() == null ? MediaType.ALL_VALUE : request.getContentType();
      this.produces = (response == null || response.getContentType() == null) ? request.getHeader("accept") : response.getContentType();
      this.params = request.getQueryString() == null ? "" : request.getQueryString();
      this.requestIp = (getRequestIp(request)).split(",")[0];
    }

    private String getRequestIp(HttpServletRequest request) {
      String clientIp = request.getHeader("X-Forwarded-For");
      if (StringUtils.isEmpty(clientIp) || "unknown".equalsIgnoreCase(clientIp)) {
        clientIp = request.getHeader("Proxy-Client-IP");
      }

      if (StringUtils.isEmpty(clientIp) || "unknown".equalsIgnoreCase(clientIp)) {
        clientIp = request.getHeader("WL-Proxy-Client-IP");
      }

      if (StringUtils.isEmpty(clientIp) || "unknown".equalsIgnoreCase(clientIp)) {
        clientIp = request.getHeader("HTTP_CLIENT_IP");
      }

      if (StringUtils.isEmpty(clientIp) || "unknown".equalsIgnoreCase(clientIp)) {
        clientIp = request.getHeader("HTTP_X_FORWARDED_FOR");
      }

      if (StringUtils.isEmpty(clientIp) || "unknown".equalsIgnoreCase(clientIp)) {
        clientIp = request.getRemoteAddr();
      }

      return clientIp;
    }

    PastePageView toEntity(Long pasteId) {
      return PastePageView.builder().pasteId(pasteId).patterns(patterns).produces(produces).requestIp(requestIp).referer(referer).build();
    }

  }


}
