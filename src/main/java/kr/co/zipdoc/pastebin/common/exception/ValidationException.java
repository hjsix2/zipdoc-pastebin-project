package kr.co.zipdoc.pastebin.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import kr.co.zipdoc.pastebin.common.exception.dto.FieldErrorDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ValidationException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final FieldErrorDto error;

  public ValidationException() {
    super("Abnormal data has been transferred. Please try again.");
    this.error = null;
  }

  public ValidationException(String message) {
    super(message);
    this.error = null;
  }

  public ValidationException(String message, FieldErrorDto error) {
    super(message);
    this.error = error;
  }

}
