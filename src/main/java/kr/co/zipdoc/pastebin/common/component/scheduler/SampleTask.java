package kr.co.zipdoc.pastebin.common.component.scheduler;

import java.io.Serializable;

import org.springframework.util.StringUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@Setter
@NoArgsConstructor
public class SampleTask implements Serializable {

  private static final long serialVersionUID = -4016593706560168341L;

  private String key;

  public SampleTask(String key) {
    this.key = key;
  }

  public boolean isKey() {
    return StringUtils.hasLength(key);
  }

}
