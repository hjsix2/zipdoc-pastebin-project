package kr.co.zipdoc.pastebin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import kr.co.zipdoc.pastebin.domain.entity.StatisticsMonthlyPastePageView;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface StatisticsMonthlyPastePageViewRepository extends JpaRepository<StatisticsMonthlyPastePageView, Long>, JpaSpecificationExecutor<StatisticsMonthlyPastePageView> {
}
