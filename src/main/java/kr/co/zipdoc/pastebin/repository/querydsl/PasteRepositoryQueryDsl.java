package kr.co.zipdoc.pastebin.repository.querydsl;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import kr.co.zipdoc.pastebin.domain.entity.Paste;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@NoRepositoryBean
public interface PasteRepositoryQueryDsl {

  List<Paste> findByExpired();

}
