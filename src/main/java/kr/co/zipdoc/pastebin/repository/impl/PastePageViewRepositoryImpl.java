package kr.co.zipdoc.pastebin.repository.impl;

import static kr.co.zipdoc.pastebin.domain.entity.QPastePageView.pastePageView;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.querydsl.core.Tuple;

import kr.co.zipdoc.pastebin.domain.entity.PastePageView;
import kr.co.zipdoc.pastebin.domain.entity.StatisticsMonthlyPastePageView;
import kr.co.zipdoc.pastebin.repository.querydsl.PastePageViewRepositoryQueryDsl;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class PastePageViewRepositoryImpl extends QuerydslRepositorySupport implements PastePageViewRepositoryQueryDsl {

  public PastePageViewRepositoryImpl() {
    super(PastePageView.class);
  }

  @Override
  public List<StatisticsMonthlyPastePageView> getMonthlyStatistics(LocalDate statisticsDate) {
    List<Tuple> tuples = from(pastePageView).select(pastePageView.pasteId, pastePageView.count())
        .where(pastePageView.createAt.between(statisticsDate.atStartOfDay(), statisticsDate.plusDays(1).atStartOfDay()))
        .groupBy(pastePageView.pasteId).fetch();

    List<StatisticsMonthlyPastePageView> statisticsMonthlyPastePageViews = new ArrayList<>();
    for (Tuple tuple : tuples) {
      statisticsMonthlyPastePageViews.add(StatisticsMonthlyPastePageView.builder()
          .pasteId(tuple.get(pastePageView.pasteId))
          .count(tuple.get(pastePageView.count()))
          .statisticsDate(statisticsDate.format(DateTimeFormatter.ofPattern("yyyy-MM")))
          .build());
    }

    return statisticsMonthlyPastePageViews;
  }

}
