package kr.co.zipdoc.pastebin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.repository.querydsl.PasteRepositoryQueryDsl;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface PasteRepository extends JpaRepository<Paste, Long>, JpaSpecificationExecutor<Paste>, PasteRepositoryQueryDsl {

}
