package kr.co.zipdoc.pastebin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import kr.co.zipdoc.pastebin.domain.entity.PastePageView;
import kr.co.zipdoc.pastebin.repository.querydsl.PastePageViewRepositoryQueryDsl;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface PastePageViewRepository extends JpaRepository<PastePageView, Long>, JpaSpecificationExecutor<PastePageView>, PastePageViewRepositoryQueryDsl {

}
