package kr.co.zipdoc.pastebin.repository.querydsl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import kr.co.zipdoc.pastebin.domain.entity.StatisticsMonthlyPastePageView;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@NoRepositoryBean
public interface PastePageViewRepositoryQueryDsl {

  List<StatisticsMonthlyPastePageView> getMonthlyStatistics(LocalDate statisticsDate);

}
