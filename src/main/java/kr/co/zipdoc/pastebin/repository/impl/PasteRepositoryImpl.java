package kr.co.zipdoc.pastebin.repository.impl;

import static kr.co.zipdoc.pastebin.domain.entity.QPaste.paste;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.repository.querydsl.PasteRepositoryQueryDsl;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public class PasteRepositoryImpl extends QuerydslRepositorySupport implements PasteRepositoryQueryDsl {

  public PasteRepositoryImpl() {
    super(Paste.class);
  }

  @Override
  public List<Paste> findByExpired() {
    return from(paste).where(paste.expirationAt.isNotNull().and(paste.expirationAt.before(LocalDateTime.now()))).fetch();
  }

}
