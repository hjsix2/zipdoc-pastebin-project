package kr.co.zipdoc.pastebin;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableCaching
@EnableScheduling
// @EnableBatchProcessing
@EnableAsync(proxyTargetClass = true)
@EnableAspectJAutoProxy(proxyTargetClass = true)
@SpringBootApplication(scanBasePackages = {"kr.co.zipdoc.pastebin"})
public class Application extends SpringBootServletInitializer implements CommandLineRunner {

  @Override
  public void run(String... args) {
    // Do nothing
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    application.sources(Application.class);
    return application;
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}

