package kr.co.zipdoc.pastebin.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import kr.co.zipdoc.pastebin.common.component.PageViewLogging;
import kr.co.zipdoc.pastebin.domain.PasteDto;
import kr.co.zipdoc.pastebin.domain.wrapper.ResourcesWrapper;
import kr.co.zipdoc.pastebin.service.PasteService;
import lombok.RequiredArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Controller
@RequestMapping(value = PasteController.RESOURCE_URI)
@RequiredArgsConstructor
public class PasteController {

  static final String RESOURCE_URI = "/pastes";

  private final PasteService service;


  @GetMapping(consumes = MediaType.ALL_VALUE, produces = MediaType.TEXT_HTML_VALUE)
  public String createPage(Model model) {

    return "content/paste-view";
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> create(@Validated @RequestBody PasteDto.Create create, UriComponentsBuilder b) {

    return ResponseEntity.created(b.path(RESOURCE_URI + "/" + service.set(create).getId()).buildAndExpand().toUri()).build();
  }

  @PageViewLogging
  @GetMapping(path = "{id}", consumes = MediaType.ALL_VALUE, produces = MediaType.TEXT_HTML_VALUE)
  public String readPage(HttpServletRequest request, HttpServletResponse response, Model model, @PathVariable Long id) {

    model.addAllAttributes(new ResourcesWrapper.Builder(PasteDto.Response.toDto(service.get(id))).toMap());

    return "content/paste-view";
  }

  @DeleteMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Void> delete(@PathVariable Long id) {

    service.remove(id);

    return ResponseEntity.noContent().build();
  }

}
