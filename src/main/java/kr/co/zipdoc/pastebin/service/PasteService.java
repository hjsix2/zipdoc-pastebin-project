package kr.co.zipdoc.pastebin.service;

import kr.co.zipdoc.pastebin.domain.PasteDto;
import kr.co.zipdoc.pastebin.domain.entity.Paste;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface PasteService {

  Paste set(PasteDto.Create create);

  Paste get(Long id);

  void remove(Long id);

  void remove();

}
