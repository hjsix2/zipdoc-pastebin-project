package kr.co.zipdoc.pastebin.service;

import java.time.LocalDate;

import kr.co.zipdoc.pastebin.domain.entity.PastePageView;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface PastePageViewService {

  PastePageView set(PastePageView pastePageView);

  void runBatch(LocalDate statisticsDate);

}
