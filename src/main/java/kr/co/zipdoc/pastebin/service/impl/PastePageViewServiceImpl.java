package kr.co.zipdoc.pastebin.service.impl;

import java.time.LocalDate;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.zipdoc.pastebin.domain.entity.PastePageView;
import kr.co.zipdoc.pastebin.repository.PastePageViewRepository;
import kr.co.zipdoc.pastebin.repository.StatisticsMonthlyPastePageViewRepository;
import kr.co.zipdoc.pastebin.service.PastePageViewService;
import lombok.RequiredArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Service
@RequiredArgsConstructor
public class PastePageViewServiceImpl implements PastePageViewService {

  private final PastePageViewRepository repository;
  private final StatisticsMonthlyPastePageViewRepository statisticsMonthlyPastePageViewRepository;


  @Transactional
  @Override
  public PastePageView set(PastePageView pastePageView) {
    return repository.save(pastePageView);
  }

  @Transactional
  @Override
  public void runBatch(LocalDate statisticsDate) {
    statisticsMonthlyPastePageViewRepository.saveAll(repository.getMonthlyStatistics(statisticsDate));
  }

}
