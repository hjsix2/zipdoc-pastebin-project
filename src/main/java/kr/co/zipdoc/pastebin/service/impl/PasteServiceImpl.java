package kr.co.zipdoc.pastebin.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kr.co.zipdoc.pastebin.common.component.MessageComponent;
import kr.co.zipdoc.pastebin.common.exception.ConflictException;
import kr.co.zipdoc.pastebin.common.exception.ResourceNotFoundException;
import kr.co.zipdoc.pastebin.domain.PasteDto;
import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.repository.PasteRepository;
import kr.co.zipdoc.pastebin.service.PasteService;
import lombok.RequiredArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Service
@RequiredArgsConstructor
public class PasteServiceImpl implements PasteService {

  private final PasteRepository repository;
  private final MessageComponent messageComponent;


  @Transactional
  @Override
  public Paste set(PasteDto.Create create) {
    return repository.save(create.toEntity());
  }

  @Transactional(readOnly = true)
  @Override
  public Paste get(Long id) {
    return repository.findById(id).orElseThrow(ResourceNotFoundException::new);
  }

  @Transactional
  @Override
  public void remove(Long id) {
    Paste paste = get(id);

    if (PasteDto.Response.toDto(paste).isExpired()) {
      repository.delete(paste);
      return;
    }

    throw new ConflictException(messageComponent.getMessage("error.Conflict.not.expired"));
  }

  @Transactional
  @Override
  public void remove() {
    repository.deleteAll(repository.findByExpired());
  }

}
