package kr.co.zipdoc.pastebin.domain.entity.convertor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
public interface CommonEnum {

  String getCode();
  String getDescription();

}
