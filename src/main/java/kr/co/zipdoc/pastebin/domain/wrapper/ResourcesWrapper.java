package kr.co.zipdoc.pastebin.domain.wrapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ResourcesWrapper {

  private Map<String, Object> content;

  public static class Builder {
    private static final String RESOURCES_KEY = "resources";
    private static final String RESOURCE_KEY = "resource";

    private ResourcesWrapper resourcesWrapper = new ResourcesWrapper();

    public Builder(Object object) {
      create(object);
    }

    public Builder content(String key, Object value) {
      if (null == resourcesWrapper.content) {
        resourcesWrapper.content = new HashMap<>();
      }

      resourcesWrapper.content.put(key, value);
      return this;
    }

    public ResourcesWrapper build() {
      return resourcesWrapper;
    }

    public Map<String, Object> toMap() {
      Map<String, Object> map = new LinkedHashMap<>();
      if (null != resourcesWrapper.content) {
        map.put("content", resourcesWrapper.content);
      }

      return map;
    }

    private void create(Object object) {
      if (null == resourcesWrapper.content) {
        resourcesWrapper.content = new HashMap<>();
      }

      resourcesWrapper.content.put(getResourceKey(object), object);
    }

    private String getResourceKey(Object resources) {
      if (resources instanceof Collection<?>) {
        return RESOURCES_KEY;
      }

      if (resources instanceof Map<?, ?>) {
        return RESOURCES_KEY;
      }

      return RESOURCE_KEY;
    }
  }

}
