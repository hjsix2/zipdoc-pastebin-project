package kr.co.zipdoc.pastebin.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@EqualsAndHashCode(callSuper = true, of = {})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "tb_paste_page_view")
public class PastePageView extends AbstractEntity {

  private static final long serialVersionUID = -8766904154330560743L;

  @Column(nullable = false)
  private Long pasteId;

  @Column(nullable = false)
  private String requestIp;

  @Column(nullable = false, length = 100)
  private String patterns;

  @Column(nullable = false, length = 1000)
  private String produces;

  private String referer;

  // TODO client 정보를 세분화 하여 조회 할 필요가 있음.
  @Builder
  public PastePageView(Long pasteId, String requestIp, String patterns, String produces, String referer) {
    this.pasteId = pasteId;
    this.requestIp = requestIp;
    this.patterns = patterns;
    this.produces = produces;
    this.referer = referer;
  }

}
