package kr.co.zipdoc.pastebin.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@EqualsAndHashCode(callSuper = true, of = {})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "tb_statistics_monthly_paste_page_view",
       uniqueConstraints = {@UniqueConstraint(name = "UK_TB_STATISTICS_MONTHLY_PASTE_PAGE_VIEW_STATISTICS_DATE_PASTE_ID", columnNames = {"statisticsDate", "pasteId"})})
public class StatisticsMonthlyPastePageView extends AbstractEntity {

  private static final long serialVersionUID = -6892159479238390996L;

  @Column(nullable = false, length = 7)
  private String statisticsDate;

  @Column(nullable = false)
  private Long pasteId;

  @Column(nullable = false)
  private Long count;


  @Builder
  public StatisticsMonthlyPastePageView(String statisticsDate, Long pasteId, Long count) {
    this.statisticsDate = statisticsDate;
    this.pasteId = pasteId;
    this.count = count;
  }

}
