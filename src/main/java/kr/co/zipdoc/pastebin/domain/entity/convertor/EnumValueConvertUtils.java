package kr.co.zipdoc.pastebin.domain.entity.convertor;

import java.util.EnumSet;

import org.apache.commons.lang3.StringUtils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EnumValueConvertUtils {

  public static <T extends Enum<T> & CommonEnum> T ofCode(Class<T> enumClass, String code) {
    if (StringUtils.isBlank(code)) {
      return null;
    }

    return EnumSet.allOf(enumClass).stream()
        .filter(e -> e.getCode().equals(code))
        .findAny()
        .orElseThrow(() -> new AssertionError(String.format("Unknown %s : %s", enumClass.getName(), code)));
  }

  public static <T extends Enum<T> & CommonEnum> T ofDescription(Class<T> enumClass, String description) {
    if (StringUtils.isBlank(description)) {
      return null;
    }

    return EnumSet.allOf(enumClass).stream()
        .filter(e -> e.getDescription().equals(description))
        .findAny()
        .orElseThrow(() -> new AssertionError(String.format("Unknown %s : %s", enumClass.getName(), description)));
  }

  public static <E extends Enum<E> & CommonEnum> String toCode(E attribute) {
    if (null == attribute) {
      return null;
    }

    return attribute.getCode();
  }

}
