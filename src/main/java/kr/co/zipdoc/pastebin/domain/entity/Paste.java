package kr.co.zipdoc.pastebin.domain.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import kr.co.zipdoc.pastebin.domain.enumeration.Expiration;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
@EqualsAndHashCode(callSuper = true, of = {})
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Table(name = "tb_paste")
public class Paste extends AbstractEntity {

  private static final long serialVersionUID = -9187251357072609922L;

  @Column(nullable = false)
  private String title;

  @Column(nullable = false, length = 4)
  @Convert(converter = Expiration.ExpirationConvertor.class)
  private Expiration expiration;

  private LocalDateTime expirationAt;

  @Column(nullable = false, columnDefinition = "TEXT")
  private String content;


  @Builder
  public Paste(String title, Expiration expiration, LocalDateTime expirationAt, String content) {
    this.title = title;
    this.expiration = expiration;
    this.expirationAt = expirationAt;
    this.content = content;
  }

}
