package kr.co.zipdoc.pastebin.domain.entity.convertor;

import javax.persistence.AttributeConverter;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
public abstract class AbstractEnumAttributeConverter<E extends Enum<E> & CommonEnum> implements AttributeConverter<E, String> {

  private final Class<E> enumClass;
  private final boolean nullable;
  private final String enumName;

  public AbstractEnumAttributeConverter(Class<E> clazz, boolean nullable, String enumName) {
    this.enumClass = clazz;
    this.nullable = nullable;
    this.enumName = enumName;
  }

  @Override
  public String convertToDatabaseColumn(E attribute) {
    if (!nullable && attribute == null) {
      throw new IllegalArgumentException(String.format("%s cannot be null", enumName));
    }

    return EnumValueConvertUtils.toCode(attribute);
  }

  @Override
  public E convertToEntityAttribute(String dbData) {
    if (!nullable && StringUtils.isBlank(dbData)) {
      throw new IllegalArgumentException(String.format("column %s is null", dbData));
    }

    return EnumValueConvertUtils.ofCode(enumClass, dbData);
  }

}
