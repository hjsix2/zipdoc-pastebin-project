package kr.co.zipdoc.pastebin.domain;

import java.time.LocalDateTime;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import kr.co.zipdoc.pastebin.domain.CommonDto;
import kr.co.zipdoc.pastebin.domain.entity.Paste;
import kr.co.zipdoc.pastebin.domain.enumeration.Expiration;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true, of = {})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PasteDto extends CommonDto {

  private static final long serialVersionUID = 2108741577396261309L;

  private String title;
  private Expiration expiration;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
  private LocalDateTime expirationAt;

  private String content;

  @SuppressWarnings("squid:S00107")
  PasteDto(Long id, String createdBy, LocalDateTime createAt, String updatedBy, LocalDateTime updateAt, String title, Expiration expiration, LocalDateTime expirationAt, String content) {
    super(id, createdBy, createAt, updatedBy, updateAt);
    this.title = title;
    this.expiration = expiration;
    this.expirationAt = expirationAt;
    this.content = content;
  }


  @Getter
  @Setter
  @NoArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Create {

    @Size(max = 255, message = "{valid.Size.message}")
    @NotBlank(message = "{valid.NotNull.message}")
    private String title;

    @NotNull(message = "{valid.NotNull.message}")
    private Expiration expiration;

    @NotBlank(message = "{valid.NotNull.message}")
    private String content;

    @Builder
    public Create(@NotBlank(message = "{valid.NotNull.message}") String title, @NotNull(message = "{valid.NotNull.message}") Expiration expiration,
        @NotBlank(message = "{valid.NotNull.message}") String content) {
      this.title = title;
      this.expiration = expiration;
      this.content = content;
    }

    public Paste toEntity() {
      return Paste.builder().title(title).expiration(expiration).expirationAt(expiration.getExpirationAt()).content(content).build();
    }

  }


  @Getter
  @Setter
  @NoArgsConstructor
  @EqualsAndHashCode(callSuper = true)
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(value = JsonInclude.Include.NON_ABSENT, content = JsonInclude.Include.NON_EMPTY)
  public static class Response extends PasteDto {

    private static final long serialVersionUID = 2950236681729033745L;

    private boolean expired;

    @Builder
    public Response(Paste paste, boolean expired) {
      super(paste.getId(), paste.getCreatedBy(), paste.getCreateAt(), paste.getUpdatedBy(), paste.getUpdateAt(), paste.getTitle(), paste.getExpiration(), paste.getExpirationAt(),
            paste.getContent());

      this.expired = expired;
    }

    public static Response toDto(Paste paste) {
      ResponseBuilder builder = Response.builder().paste(paste);
      if (paste.getExpiration() != Expiration.NAVER) {
        builder.expired(paste.getExpirationAt().isAfter(LocalDateTime.now()));
      }

      return builder.build();
    }

  }

}
