package kr.co.zipdoc.pastebin.domain.enumeration;

import java.time.LocalDateTime;
import java.util.stream.Stream;

import javax.persistence.Converter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import kr.co.zipdoc.pastebin.domain.entity.convertor.AbstractEnumAttributeConverter;
import kr.co.zipdoc.pastebin.domain.entity.convertor.CommonEnum;
import lombok.Getter;

/**
 * @author hjsix2
 * @since 0.0.1-SNAPSHOT
 */
@Getter
public enum Expiration implements CommonEnum {

  NAVER("0000", "Naver") {
    @Override
    public LocalDateTime getExpirationAt() {
      return null;
    }
  }, TEN_MINUTE("m010", "10 Minutes") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusMinutes(10);
    }
  }, ONE_HOUR("H001", "1 Hour") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusHours(1);
    }
  }, ONE_DAY("d001", "1 Day") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusDays(1);
    }
  }, ONE_WEEK("W001", "1 Week") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusWeeks(1);
    }
  }, TWO_WEEK("W001", "2 Weeks") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusWeeks(2);
    }
  }, ONE_MONTH("M001", "1 Month") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusMonths(1);
    }
  }, SIX_MONTH("M006", "6 Months") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusMonths(6);
    }
  }, ONE_YEAR("y001", "1 Year") {
    @Override
    public LocalDateTime getExpirationAt() {
      return LocalDateTime.now().plusYears(1);
    }
  };

  @JsonValue
  private String code;

  private String description;

  Expiration(String code, String description) {
    this.code = code;
    this.description = description;
  }

  @JsonCreator
  public static Expiration findByCode(String code) {
    return Stream.of(Expiration.values()).filter(e -> e.getCode().equalsIgnoreCase(code)).findFirst().orElse(Expiration.NAVER);
  }

  @Converter(autoApply = false)
  public static class ExpirationConvertor extends AbstractEnumAttributeConverter<Expiration> {

    private static final String ENUM_NAME = "Paste Expiration";

    public ExpirationConvertor() {
      super(Expiration.class, false, ENUM_NAME);
    }
  }

  abstract public LocalDateTime getExpirationAt();

}
