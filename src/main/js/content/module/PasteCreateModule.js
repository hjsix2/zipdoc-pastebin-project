import RestService from '../../common/RestService';

const PasteCreateModule = ((RestService) => {
  'use strict';
  let _form = document.getElementById('form');

  const create = async() => {
    const url = _form.getAttribute('action');

    toggleRequestStatus(true);

    const data = {
      content: _form.content.value,
      expiration: _form.expiration.value,
      title: _form.title.value
    };

    RestService.create({
      url,
      data,
      success: function(data, status, xhr) {
        // eslint-disable-next-line no-undef
        alert('등록 되었습니다.');
        location.href = xhr.getResponseHeader('Location');
      },
      async error(jqXHR) {
        if (jqXHR.status === 400 && Array.isArray(jqXHR.errors) && jqXHR.errors.length > 0) {
          const error = jqXHR.errors[0];
          
          // eslint-disable-next-line no-undef
          alert(error.message);
          _form[error.field].focus();

          return;
        }

        // eslint-disable-next-line no-undef
        alert(jqXHR);
      },
      complete() {
        removeElement('.remove_input');
        toggleRequestStatus(false);
      }
    });
  };

  const removeElement = selecter => {
    Array.from(document.querySelectorAll(selecter)).forEach(_element => {
      _element.parentNode.removeChild(_element);
    });
  };

  const toggleRequestStatus = isRequest => {
    const _expiration = document.getElementById('expiration');
    const _submitButton = document.getElementById('submit_button');
    if (isRequest) {
      _expiration.setAttribute('disabled', 'disabled');
      _submitButton.setAttribute('disabled', 'disabled');
    } else {
      _expiration.removeAttribute('disabled');
      _submitButton.removeAttribute('disabled');
    }
  };

  return {
    create
  };
})(RestService);

export default PasteCreateModule;
