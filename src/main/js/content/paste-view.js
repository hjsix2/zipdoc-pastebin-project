import PasteCreateModule from './module/PasteCreateModule';

document.addEventListener('DOMContentLoaded', () => {
  document.addEventListener('click', e => {
    const _element = e.target;
    if (!_element) {
      return;
    }

    if (_element.id === 'submit_button' || _element.parentNode.id === 'submit_button') {
      PasteCreateModule.create();
      return;
    }
  });
});
