const RestService = (($) => {
  'use strict';

  const create = options => {
    options.type = 'POST';
    options.dataType = 'json';
    send(options);
  };

  const send = async options => {
    if (!options) {
      throw '{options} 를 지정해야 합니다.';
    }
    
    if (!options.url) {
      throw 'url 이 지정되지 않았습니다.';
    }

    let option = {
      url: options.url,
      success(data, textStatus, jqXHR) {
        onSuccess(options, data, textStatus, jqXHR);
      },
      error(jqXHR) {
        onError(options, jqXHR);
      },
      complete(jqXhr, textStatus) {
        if (options.complete) {
          options.complete(jqXhr, textStatus);
        }
      }
    };

    let data = options.data;
    let contentType = options.contentType === undefined ? 'application/json; charset=UTF-8' : options.contentType;

    if (options.type !== 'GET') {
      data = JSON.stringify(options.data);
    }

    option = Object.assign(option, {
      method: options.type,
      data,
      contentType
    });

    console.log(option);

    $.ajax(option);
  };

  const onSuccess = async(options, data, textStatus, jqXHR, _form) => {
    if (options.success) {
      options.success(data, textStatus, jqXHR, _form);
    }
  };
  
  const onError = async(options, jqXHR) => {
    error(options, jqXHR);
  };
  
  const error = (options, jqXHR) => {
    if (jqXHR.status === 500) {
      try {
        // eslint-disable-next-line no-undef
        alert('요청하신 페이지에 문제가 있어 페이지를 표시할 수 없습니다.');
      } catch (e) {
        // Do Nothing
      }
      return;
    }

    if (jqXHR.status === 409 && jqXHR.responseJSON.code) {
      /** @namespace jqXHR.responseJSON */
      if (jqXHR.responseJSON.code === 501) {
        // eslint-disable-next-line no-undef
        alert('error', jqXHR.responseJSON.message);
      }
    } else if (jqXHR.status === 413) {
      // eslint-disable-next-line no-undef
      alert('요청하신 데이터가 너무 커서 서버가 처리할 수 없습니다.');
    } else {
      if (options.error) {
        options.error(jqXHR.responseJSON);
      }
    }
  };

  return {
    create
  };
})(jQuery);

export default RestService;
